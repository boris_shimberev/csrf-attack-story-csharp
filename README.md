# Cross-Site Request Forgery Prevention Story

**To read**: [https://cheatsheetseries.owasp.org/cheatsheets/Cross-Site_Request_Forgery_Prevention_Cheat_Sheet.html]

**Estimated reading time**: 12 min

## Story Outline

This story is focused on providing guidance for preventing a special case of
Cross-Site Request Forgery in your applications. The story consists of an example
of a modern CSRF vulnerability of a web form and a theoretical information about
an attack and ways of a prevention.

Cross-Site Request Forgery (CSRF) is an attack that forces an end user to execute
unwanted actions on a web application in which they're currently authenticated.
With a little help of social engineering (such as sending a link via email or
chat), an attacker may trick the users of a web application into executing actions
of the attacker's choosing. If the victim is a normal user, a successful CSRF
attack can force the user to perform state changing requests like transferring
funds, changing their email address, and so forth. If the victim is administrative
account, CSRF can compromise the entire web application. Another definition of the
process is a session hijacking or a session riding.

## Story Organization

**Story Branch**: master
>`git checkout master`

**Practical task tag for self-study**: task
>`git checkout task`

Tags: #csrf, #security
