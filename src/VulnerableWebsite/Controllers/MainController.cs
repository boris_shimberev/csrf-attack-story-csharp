using Microsoft.AspNetCore.Mvc;
using VulnerableWebsite.Providers;

namespace VulnerableWebsite.Controllers;

public class MainController: Controller
{
    [Route("/")]
    public IActionResult Index()
    {
        var session = SessionProvider.GetFromHttpContext(HttpContext);
        return session.HasValue ? Redirect("status") : Redirect("login");
    }

    [Route("login")]
    public IActionResult Login()
    {
        var session = SessionProvider.GetFromHttpContext(HttpContext);
        ViewBag.Session = session;
        return session.HasValue ? Redirect("status") : View();
    }

    [Route("status")]
    public IActionResult Status()
    {
        var session = SessionProvider.GetFromHttpContext(HttpContext);

        if (!session.HasValue)
        {
            return Redirect("login");
        }

        ViewBag.Session = session;
        return View();
    }
}