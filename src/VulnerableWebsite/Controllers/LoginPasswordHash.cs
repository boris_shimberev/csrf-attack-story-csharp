namespace VulnerableWebsite.Controllers;

public class LoginPasswordHash
{
    public string LoginHash { get; set; }
    public string PasswordHash { get; set; }
}