using Microsoft.AspNetCore.Mvc;
using VulnerableWebsite.Providers;

namespace VulnerableWebsite.Controllers;

[AutoValidateAntiforgeryToken]
public class FormsController: Controller
{
    [HttpPost]
    [Route("try-login")]
    public IActionResult TryLogin([FromForm] LoginPasswordHash loginInfo)
    {
        // Login
        var isLoginOk =
            loginInfo.LoginHash == "9d6322c1f4d9d3f38aed8bfbe0b2bcadf66ad82c008476fa62541fa069138e94";
        // Password
        var isPasswordOk =
            loginInfo.PasswordHash == "e7cf3ef4f17c3999a94f2c6f612e8a888e5b1026878e4e19398b23bd38ec221a";

        if (!isLoginOk || !isPasswordOk)
        {
            return Redirect("login");
        }

        ViewBag.Session = new Session(loginInfo.LoginHash);

        SessionProvider.SetSession(ViewBag.Session);
        SessionProvider.SetSessionCookie(HttpContext, ViewBag.Session);

        return Redirect("status");
    }

    [HttpPost]
    [Route("try-logout")]
    public IActionResult TryLogout()
    {
        var session = SessionProvider.GetFromHttpContext(HttpContext);

        if (!session.HasValue)
        {
            return Redirect("/");
        }

        SessionProvider.RemoveSession(session.Value);
        SessionProvider.RemoveSessionCookie(HttpContext);

        return Redirect("/");
    }

    [HttpPost]
    [Route("new-status")]
    public IActionResult NewStatus([FromForm] NewStatusCommand newStatusCommand)
    {
        var session = SessionProvider.GetFromHttpContext(HttpContext);

        if (!session.HasValue)
        {
            return Redirect("login");
        }

        StatusProvider.SetStatus(session.Value.Login, newStatusCommand.Status);

        return Redirect("status");
    }
}