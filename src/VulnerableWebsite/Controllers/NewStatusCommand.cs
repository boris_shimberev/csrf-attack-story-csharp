namespace VulnerableWebsite.Controllers;

public class NewStatusCommand
{
    public string Status { get; set; }
}