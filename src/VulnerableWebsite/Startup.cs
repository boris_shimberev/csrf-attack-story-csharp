using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace VulnerableWebsite;

public class Startup
{
    public void ConfigureServices(IServiceCollection services)
        => services
            .AddControllersWithViews()
            .AddRazorRuntimeCompilation();

    public void Configure(IApplicationBuilder app)
        => app.UseRouting().UseEndpoints(e => e.MapControllers());
}