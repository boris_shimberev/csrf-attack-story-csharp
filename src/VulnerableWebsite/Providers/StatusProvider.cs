using System.Collections.Generic;

namespace VulnerableWebsite.Providers;

public static class StatusProvider
{
    private static readonly Dictionary<string, string> Storage = new();

    public static string GetStatus(string user) => Storage.GetValueOrDefault(user);

    public static void SetStatus(string user, string newStatus)
    {
        if (Storage.ContainsKey(user))
        {
            Storage[user] = newStatus;
        }
        else
        {
            Storage.Add(user, newStatus);
        }
    }
}