using System;

namespace VulnerableWebsite.Providers;

public readonly struct Session
{
    public Session(string loginHash)
    {
        SessionId = Guid.NewGuid();
        Login = loginHash;
        StartDateTime = DateTime.UtcNow;
    }

    public Guid SessionId { get; }

    public string Login { get; }

    public DateTime StartDateTime { get; }
}