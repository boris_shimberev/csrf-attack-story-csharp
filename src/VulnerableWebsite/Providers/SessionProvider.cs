using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace VulnerableWebsite.Providers;

public static class SessionProvider
{
    public const int SessionDurationSecs = 20;

    private const string SessionIdHeader = "__Host-SessionId";

    private static readonly Dictionary<Guid, Session> Storage = new();

    public static void SetSession(Session session) => Storage.TryAdd(session.SessionId, session);

    public static void RemoveSession(Session session) => Storage.Remove(session.SessionId);

    public static Session? GetFromHttpContext(HttpContext httpContext)
    {
        httpContext.Request.Cookies.TryGetValue(SessionIdHeader, out var sessionIdString);

        var session = Guid.TryParse(sessionIdString, out var sessionId)
            ? GetSession(sessionId)
            : null;

        if (!session.HasValue)
        {
            return null;
        }

        if (session.Value.StartDateTime > DateTime.UtcNow.AddSeconds(-SessionDurationSecs))
        {
            return session;
        }

        RemoveSession(session.Value);

        return null;
    }

    public static void SetSessionCookie(HttpContext httpContext, Session session)
        => httpContext.Response.Cookies.Append(
            SessionIdHeader,
            session.SessionId.ToString(),
            new CookieOptions
            {
                SameSite = SameSiteMode.Strict,
                HttpOnly = true,
                Expires = DateTimeOffset.UtcNow.AddSeconds(SessionDurationSecs),
                Secure = true
            });

    public static void RemoveSessionCookie(HttpContext httpContext)
        => httpContext.Response.Cookies.Delete(SessionIdHeader);

    private static Session? GetSession(Guid sessionId)
        => Storage.TryGetValue(sessionId, out var session) ? session : null;
}